/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
let url = serverurl;

const vm = new Vue({
    el: 'main',
    data: {
        paisaux: {},
        areaaux: {},
        tipodocaux: {}

    },
    methods: {
        registroEmpleado: registroEmpleado
    },
    created: cargaDatos()
});

function registroEmpleado() {


    let primnomb = document.getElementById("primerNombre").value;
    let otronom = document.getElementById("otrosNombres").value;
    let primapell = document.getElementById("primerApellido").value;
    let segapell = document.getElementById("segundoApellido").value;
    let numid = document.getElementById("numeroIdentificacion").value;
    let tipodoc = document.getElementById("tipoDocumento").value;
    let pais = document.getElementById("pais").value;
    let area = document.getElementById("area").value;
    let estado = document.getElementById("estado").value;
    let fecha = document.getElementById("fechaIngreso").value;

    let select = new Date(fecha);
    let comp = new Date();
    comp.setMonth(comp.getMonth() - 1);

    if (select < comp) {
        console.log("fecha no valida");
        return;
    }

    let data = {
        primerNombre: primnomb,
        otrosNombres: otronom,
        primerApellido: primapell,
        segundoApellido: segapell,
        numeroIdentificacion: numid,
        pais: pais,
        correo: '',
        tipoDocumento: tipodoc,
        area: area,
        estado: estado,
        fechaIngreso: fecha
    };

    console.log(data);

    let init = makeinit(data);

    let admurlc = url + 'empleado/new';

    fetch(admurlc, init)
            .then((resp) => resp.json())
            .then(function (data) {

                if (data.mensaje === "ok") {
                    limpiarFormulario();
                    alert("registro exitoso");

                } else {

                    let mens = [];

                    for (let err = 0; err < data.errors.length; err++) {
                        mens.push(
                                data.errors[err].defaultMessage);
                    }
                    alert(mens.toString());

                }


            });

}


function limpiarFormulario() {

    //document.getElementById("tipoDocumento").value = data.tipoDocumento;
    document.getElementById("pais").selected = 2;

    //document.getElementById("area").value = data.area;
    //document.getElementById("estado").value = data.estado;
    //document.getElementById("fechaIngreso").value =;

    document.getElementById("primerNombre").value = "";
    document.getElementById("otrosNombres").value = "";
    document.getElementById("primerApellido").value = "";
    document.getElementById("segundoApellido").value = "";
    document.getElementById("numeroIdentificacion").value = "";
    //document.getElementById("tipoDocumento").value = data.tipoDocumento;
    //document.getElementById("pais").selected = 2;


    //document.getElementById("area").value = data.area;
    //document.getElementById("estado").value = data.estado;
}

function cargaDatos() {
    cargaArea();
    cargaPaises();
    cargatipodocumento();
}


function cargaArea() {
    let init = makeinitnodat();
    let admurlc = url + 'area/listar';
    fetch(admurlc, init)
            .then((resp) => resp.json())
            .then(function (data) {
                console.log(data);
                vm.areaaux = data.lista;
            });
}

function cargaPaises() {

    let init = makeinitnodat();
    let admurlc = url + 'pais/listar';
    fetch(admurlc, init)
            .then((resp) => resp.json())
            .then(function (data) {
                console.log(data);
                vm.paisaux = data.lista;
            });
}


function cargatipodocumento() {


    let init = makeinitnodat();
    let admurlc = url + 'documento/listar';
    fetch(admurlc, init)
            .then((resp) => resp.json())
            .then(function (data) {
                console.log(data);
                vm.tipodocaux = data.lista;
            });
}


function makeinit(data) {
    let heads = new Headers();
    heads.append("Accept", "application/json");
    heads.append("Content-Type", "application/json");
    heads.append("Access-Control-Allow-Origin", '*');
    let init = {
        method: 'POST',
        mode: 'cors',
        body: JSON.stringify(data),
        headers: heads
    };
    return init;
}

function makeinitnodat() {
    let heads = new Headers();
    heads.append("Accept", "application/json");
    heads.append("Content-Type", "application/json");
    heads.append("Access-Control-Allow-Origin", '*');
    let init = {
        method: 'POST',
        mode: 'cors',
        headers: heads
    };
    return init;
}

function strdate(d) {
    return strDate = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();


}