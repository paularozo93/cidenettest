let url = serverurl;
$(document).ready(function () {
    const vm = new Vue({
        el: 'main',
        data: {
            empleados: {},
            idaux: 0,
            paisaux: {},
            areaaux: {},
            tipodocaux: {}

        },
        methods: {
            cargaEmpleados: cargaEmpleados,
            actualizarEmpleado: actualizarEmpleado
        },
        created: cargaEmpleados,
    });

    let table = null;


    function actualizarEmpleado() {

        let id = vm.idaux;
        console.log("ed empleado")
        console.log(vm.idaux);

        let primnomb = document.getElementById("primerNombre").value;
        let otronom = document.getElementById("otrosNombres").value;
        let primapell = document.getElementById("primerApellido").value;
        let segapell = document.getElementById("segundoApellido").value;
        let numid = document.getElementById("numeroIdentificacion").value;
        let tipodoc = document.getElementById("tipoDocumento").value;
        let pais = document.getElementById("pais").value;

        document.getElementById("correo").value;
        let area = document.getElementById("area").value;
        let estado = document.getElementById("estado").value;
        let fecha = document.getElementById("fechaIngreso").value;

        let select = new Date(fecha);
        let comp = new Date();
        comp.setMonth(comp.getMonth() - 1);
        strdate(comp);
        strdate(select);

        if (select < comp) {
            console.log("fecha no valida");
            //return;
        }
        let fechaingres = strdate(select);
        console.log(fechaingres);

        let data = {
            id: id,
            primerNombre: primnomb,
            otrosNombres: otronom,
            primerApellido: primapell,
            segundoApellido: segapell,
            numeroIdentificacion: numid,
            pais: pais,
            correo: '',
            tipoDocumento: tipodoc,
            area: area,
            estado: estado,
            fechaIngreso: fecha

        };

        console.log(data);

        let init = makeinit(data)
        let admurlc = url + 'empleado/edit';
        fetch(admurlc, init)
                .then((resp) => resp.json())
                .then(function (data) {

                    if (data.mensaje === "ok") {
                        limpiarFormulario();
                        alert("edicion exitosa");
                        location.reload();

                    } else {

                        let mens = [];

                        for (let err = 0; err < data.errors.length; err++) {
                            mens.push(data.errors[err].defaultMessage);
                        }
                        alert(mens.toString());
                    }
                });

    }

    function cargaArea() {
        let init = makeinitnodat()
        let admurlc = url + 'area/listar';
        fetch(admurlc, init)
                .then((resp) => resp.json())
                .then(function (data) {
                    console.log(data);
                    vm.areaaux = data.lista;
                });
    }

    function cargaPaises() {

        let init = makeinitnodat();
        let admurlc = url + 'pais/listar';
        fetch(admurlc, init)
                .then((resp) => resp.json())
                .then(function (data) {
                    console.log(data);
                    vm.paisaux = data.lista;
                });
    }


    function cargatipodocumento() {

        let init = makeinitnodat();
        let admurlc = url + 'documento/listar';
        fetch(admurlc, init)
                .then((resp) => resp.json())
                .then(function (data) {
                    console.log(data);
                    vm.tipodocaux = data.lista;
                });
    }

    function cargaEmpleadoEspecifico() {

        let id = vm.idaux

        if (id == 0) {
            console.log("vacio");
            return;
        }

        data = {
            value1: id,
            value2: '0'
        };

        let init = makeinit(data);
        let admurlc = url + 'empleado/find';
        fetch(admurlc, init)
                .then((resp) => resp.json())
                .then(function (data) {
                    console.log(data);

                    llenarFormulario(data);

                });

    }


    function borrarEmpleado(id) {

        data = {
            value1: id,
            value2: '0'
        };


        let init = makeinit(data);

        let admurlc = url + 'empleado/delete';
        fetch(admurlc, init)
                .then((resp) => resp.json())
                .then(function (data) {
                    console.log(data);
                    vm.idaux = 0;
                });

    }

    function limpiarFormulario() {
        document.getElementById("primerNombre").value = "";
        document.getElementById("otrosNombres").value = "";
        document.getElementById("primerApellido").value = "";
        document.getElementById("segundoApellido").value = "";
        document.getElementById("numeroIdentificacion").value = "";
        //document.getElementById("tipoDocumento").value = data.tipoDocumento;
        //document.getElementById("pais").selected = 2;


        document.getElementById("correo").value = "";
        //document.getElementById("area").value = data.area;
        //document.getElementById("estado").value = data.estado;
    }

    function llenarFormulario(data) {
        document.getElementById("primerNombre").value = data.primerNombre;
        document.getElementById("otrosNombres").value = data.otrosNombres;
        document.getElementById("primerApellido").value = data.primerApellido;
        document.getElementById("segundoApellido").value = data.segundoApellido;
        document.getElementById("numeroIdentificacion").value = data.numeroIdentificacion;

        let tipos = vm.tipodocaux;

        for (let i = 0; i < tipos.length; i++) {

            if (tipos[i].nombre.localeCompare(data.tipoDocumento) === 0) {

                $("#tipoDocumento").val(tipos[i].id).change();

            }

        }

        let paises = vm.paisaux;
        for (let i = 0; i < paises.length; i++) {

            if (paises[i].nombre.localeCompare(data.pais) === 0) {

                $("#pais").val(paises[i].id).change();

            }

        }
        document.getElementById("correo").value = data.correo;


        let areas = vm.areaaux;

        for (let i = 0; i < areas.length; i++) {

            if (areas[i].nombre.localeCompare(data.area) === 0) {

                $("#area").val(areas[i].id).change();

            }

        }

        $("#estado").val(data.estado).change();
        //document.getElementById("area").value = data.area;
        //document.getElementById("estado").value = data.estado;
        document.getElementById("fechaIngreso").value = data.fechaIngreso;
    }


    function cargaEmpleados() {
        console.log("carga empleados");

        let init = makeinitnodat();
        cargaArea();
        cargaPaises();
        cargatipodocumento();
        let admurlc = url + 'empleado/list';
        fetch(admurlc, init)
                .then((resp) => resp.json())
                .then(function (data) {

                    let datos = [];
                    for (let i = 0; i < data.lista.length; i++) {

                        console.log(data.lista[i]);

                        let arr = [
                            data.lista[i].primerNombre,
                            data.lista[i].otrosNombres,
                            data.lista[i].primerApellido,
                            data.lista[i].segundoApellido,
                            data.lista[i].numeroIdentificacion,
                            data.lista[i].tipoDocumento,
                            data.lista[i].pais,
                            data.lista[i].correo,
                            data.lista[i].area,
                            data.lista[i].estado,
                            data.lista[i].fechaIngreso,
                            data.lista[i].fechaHoraRegistro,
                            data.lista[i].fechaHoraActualizacion,
                            data.lista[i].id
                        ];
                        datos.push(arr);

                    }
                    console.log(datos);




                    function displaySearch() {
                        table = $('#table1').DataTable();
                        console.log("TABLE = " + table.search());
                        for (var i = 0; i < table.columns().count(); i++) {
                            console.log("Column " + i + ": " + table.column(i).search());
                        }
                    }


                    //genera la tabla
                    table = $('#table1').DataTable({
                        data: datos, //arreglo con los datos de la tabla
                        "pagingType": "full_numbers", //especificaciond de tipo de paginado
                        "order": [[3, "desc"]], //permitir invertir filas
                        "bLengthChange": false, //ocultar campo de mostrar ciertos campos
                        //"bFilter": false, //ocultar campo de busqueda
                        orderCellsTop: true,
                        stateSave: true,
                        //realiza la busqueda

                        columns: [
                            {title: "primerNombre"},
                            {title: "otrosNombres"},
                            {title: "primerApellido"},
                            {title: "segundoApellido"},
                            {title: "numeroIdentificacion"},
                            {title: "tipoDocumento"},
                            {title: "pais"},
                            {title: "correo"},
                            {title: "area"},
                            {title: "estado"},
                            {title: "fechaIngreso"},
                            {title: "fechaHoraRegistro"},
                            {title: "fechaHoraActualizacion"},
                            {title: "id"}
                        ], //declaracion de las columnas de la tabla
                        "columnDefs": [
                            {
                                "targets": [11],
                                "visible": false,
                            },
                            {
                                "targets": [12],
                                "visible": false
                            },
                            {
                                "targets": [13],
                                "visible": false
                            }], //declaracion de columnas ocultas de la tabla

                        initComplete: function () {
                            // Apply the search
                            let table = this.api();
                            table.columns().every(function () {
                                let column = this;

                                let input = $('<input type="text" />')
                                        .appendTo($("thead tr:eq(1) td").eq(this.index()))
                                        .on("keyup", function () {
                                            column.search($(this).val()).draw();
                                        });

                                // Restore state saved values
                                let state = this.state.loaded();
                                if (state) {
                                    let val = state.columns[this.index()];
                                    input.val(val.search.search);
                                }

                            });
                            displaySearch();
                        }
                    });

                    table.on('search.dt', function () {
                        displaySearch();
                    });
                    //evento de la tabla al clickar en ella
                    $('#table1 tbody').on('click', 'tr', function () {

                        if ($(this).hasClass('selected')) {
                            $(this).removeClass('selected');
                            vm.idaux = 0;
                            console.log("deselect");
                            vm.empleadoaux = {};
                            limpiarFormulario();
                        } else {
                            table.$('tr.selected').removeClass('selected');
                            $(this).addClass('selected');
                            var data = table.row(this).data();
                            let idempleado = data[13];
                            vm.idaux = idempleado;
                            console.log(idempleado);
                        }
                    });

                    $('#button').click(function () {
                        table.row('.selected').remove().draw(false);
                        if (confirm('desea borrar el empleado?')) {
                            if (vm.idaux > 0) {
                                borrarEmpleado(vm.idaux);
                            }
                            alert("seleccione un empleado a eliminar");
                        }
                    });

                    $('#button2').click(function () {

                        if (vm.idaux < 1) {
                            alert("seleccione un empleado a editar");
                        }
                        cargaEmpleadoEspecifico();


                    });





                });
    }


});


function makeinit(data) {
    let heads = new Headers();
    heads.append("Accept", "application/json");
    heads.append("Content-Type", "application/json");
    heads.append("Access-Control-Allow-Origin", '*');
    let init = {
        method: 'POST',
        mode: 'cors',
        body: JSON.stringify(data),
        headers: heads
    };
    return init;
}

function makeinitnodat() {
    let heads = new Headers();
    heads.append("Accept", "application/json");
    heads.append("Content-Type", "application/json");
    heads.append("Access-Control-Allow-Origin", '*');
    let init = {
        method: 'POST',
        mode: 'cors',
        headers: heads
    };
    return init;
}

function strdate(d) {
    return strDate = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();

}