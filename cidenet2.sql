-- Adminer 4.8.1 MySQL 5.5.5-10.6.3-MariaDB dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `area`;
CREATE TABLE `area` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `area` (`id`, `nombre`) VALUES
(1,	'administracion'),
(2,	'financiera'),
(3,	'Compras'),
(4,	'Infraestructura'),
(5,	'Operacion'),
(6,	'Talento humano'),
(7,	'servicios varios');

DROP TABLE IF EXISTS `pais`;
CREATE TABLE `pais` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `dominio` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `pais` (`id`, `nombre`, `dominio`) VALUES
(1,	'colombia',	'cidenet.com.co'),
(2,	'estados unidos',	'cidenet.com.us');

DROP TABLE IF EXISTS `tipo_documento`;
CREATE TABLE `tipo_documento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tipo_documento` (`id`, `nombre`) VALUES
(1,	'cedula de ciudadania'),
(2,	'cedula de extranjeria'),
(3,	'pasaporte'),
(4,	'permiso especial');




DROP TABLE IF EXISTS `empleado`;
CREATE TABLE `empleado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `primer_apellido` varchar(20) NOT NULL,
  `segundo_apellido` varchar(20) NOT NULL,
  `primer_nombre` varchar(20) NOT NULL,
  `otros_nombres` varchar(50) DEFAULT NULL,
  `numero_identificacion` varchar(20) NOT NULL,
  `correo` varchar(300) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fecha_ingreso` date NOT NULL,
  `fecha_hora_registro` datetime NOT NULL,
  `fecha_hora_actualizacion` datetime DEFAULT NULL,
  `id_area` int(11) NOT NULL,
  `id_pais` int(11) NOT NULL,
  `id_documento` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `empleado_area` (`id_area`),
  KEY `empleado_pais` (`id_pais`),
  KEY `empleado_tipo_documento` (`id_documento`),
  CONSTRAINT `empleado_area` FOREIGN KEY (`id_area`) REFERENCES `area` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `empleado_pais` FOREIGN KEY (`id_pais`) REFERENCES `pais` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `empleado_tipo_documento` FOREIGN KEY (`id_documento`) REFERENCES `tipo_documento` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `empleado` (`id`, `primer_apellido`, `segundo_apellido`, `primer_nombre`, `otros_nombres`, `numero_identificacion`, `correo`, `estado`, `fecha_ingreso`, `fecha_hora_registro`, `fecha_hora_actualizacion`, `id_area`, `id_pais`, `id_documento`) VALUES
(1,	'ORTIZ',	'ORDOAEZ',	'PEPE',	'JUAN MALDONADO',	'12345-0',	'PEPE.ORTIZ@cidenet.com.co',	1,	'2021-01-11',	'2021-07-28 16:00:09',	NULL,	1,	1,	1),
(3,	'ORTIZ',	'ORTEGA',	'ESTEBANO',	'JUAN',	'12345-2',	'ESTEBANO.ORTIZ@cidenet.com.us',	1,	'2021-01-13',	'2021-07-29 17:47:49',	'2021-07-29 17:47:49',	3,	2,	1),
(12,	'ORTIZ',	'MALDONADO',	'JUANITA',	'JUANA ALGUIEN',	'1234',	'JUANITA.ORTIZ@cidenet.com.co',	1,	'2021-01-28',	'2021-07-29 19:53:10',	'2021-07-29 19:53:11',	1,	1,	1),
(13,	'MANO',	'MALDONADOO',	'MANUEL',	'SD ER',	'213',	'MANUEL.MANO@cidenet.com.co',	1,	'2021-01-21',	'2021-07-29 21:34:22',	NULL,	4,	1,	1),
(14,	'TRES',	'CUATRO',	'CES',	'DOS',	'343-AS',	'CES.TRES@cidenet.com.co',	1,	'2021-01-27',	'2021-07-29 21:35:42',	NULL,	1,	1,	1),
(15,	'JUANESA',	'JUANOSA',	'JUANA',	'JUNITA',	'676',	'JUANA.JUANESA@cidenet.com.co',	1,	'2021-01-27',	'2021-07-29 21:36:03',	NULL,	1,	1,	1),
(16,	'BIBI',	'BBU',	'BABA',	'BEBE',	'355455',	'BABA.BIBI@cidenet.com.us',	1,	'2021-01-27',	'2021-07-29 21:36:24',	NULL,	3,	2,	1),
(17,	'PEREZ',	'PEREZ',	'LILI',	'',	'3232',	'LILI.PEREZ@cidenet.com.co',	1,	'2021-01-13',	'2021-07-29 21:36:59',	NULL,	1,	1,	1),
(18,	'PEPE',	'ALGO',	'PEPEMAN',	'ES',	'87554',	'PEPEMAN.PEPE@cidenet.com.co',	1,	'2021-01-13',	'2021-07-29 21:37:14',	NULL,	1,	1,	1),
(19,	'ASD',	'ASSASAAS',	'PO',	'',	'23423',	'PO.ASD@cidenet.com.co',	1,	'2021-01-13',	'2021-07-29 21:37:27',	NULL,	1,	1,	1),
(20,	'DS',	'GAGAGAGAGA',	'DDDSS',	'',	'232',	'DDDSS.DS@cidenet.com.co',	1,	'2021-01-13',	'2021-07-29 21:37:39',	NULL,	1,	1,	1);

-- 2021-07-29 21:49:32 
