/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.empleados.service.impl;

import com.cidenet.empleados.dto.AreaDTO;
import com.cidenet.empleados.dto.json.ListaJson;
import com.cidenet.empleados.entity.Area;
import com.cidenet.empleados.repository.AreaRepository;
import com.cidenet.empleados.service.AreaService;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/**
 *
 * @author jerson
 */
@Service
public class AreaServiceImpl implements AreaService {

    @Autowired
    private AreaRepository areaRepository;

    /**
     * 
     * @return ResponseEntity con la lista de Areas
     */
    @Override
    public ResponseEntity ListAll() {
        ArrayList<Area> areas = (ArrayList<Area>) areaRepository.findAll();
        ArrayList<AreaDTO> retorno = new ArrayList<AreaDTO>();
        ListaJson<AreaDTO> lista = new ListaJson<AreaDTO>();

        for (Area a : areas) {
            AreaDTO adto = new AreaDTO();
            adto.setId("" + a.getId());
            adto.setNombre(a.getNombre());
            retorno.add(adto);
        }
        lista.setLista(retorno);
        return ResponseEntity.ok(lista);
    }

}
