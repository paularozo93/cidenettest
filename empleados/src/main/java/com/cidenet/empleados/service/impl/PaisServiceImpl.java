/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.empleados.service.impl;

import com.cidenet.empleados.dto.PaisDTO;
import com.cidenet.empleados.dto.json.ListaJson;
import com.cidenet.empleados.entity.Pais;
import com.cidenet.empleados.repository.PaisRepository;
import com.cidenet.empleados.service.PaisService;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/**
 *
 * @author jerson
 */
@Service
public class PaisServiceImpl implements PaisService {

    @Autowired
    private PaisRepository paisRepository;

    /**
     * 
     * @return ResponseEntity con la lista de paises
     */
    @Override
    public ResponseEntity ListAll() {
        ArrayList<Pais> paises = (ArrayList<Pais>) paisRepository.findAll();
        ArrayList<PaisDTO> retorno = new ArrayList<PaisDTO>();
        ListaJson<PaisDTO> lista = new ListaJson<PaisDTO>();

        for (Pais p : paises) {
            PaisDTO pdto = new PaisDTO();
            pdto.setId("" + p.getId());
            pdto.setNombre(p.getNombre());
            retorno.add(pdto);

        }
        lista.setLista(retorno);
        return ResponseEntity.ok(lista);
    }

}
