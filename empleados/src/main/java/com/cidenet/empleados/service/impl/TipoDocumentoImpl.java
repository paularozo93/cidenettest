/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.empleados.service.impl;

import com.cidenet.empleados.dto.PaisDTO;
import com.cidenet.empleados.dto.TipoDocumentoDTO;
import com.cidenet.empleados.dto.json.ListaJson;
import com.cidenet.empleados.entity.TipoDocumento;
import com.cidenet.empleados.repository.TipoDocumentoRepository;
import com.cidenet.empleados.service.TipoDocumentoService;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/**
 *
 * @author jerson
 */
@Service
public class TipoDocumentoImpl implements TipoDocumentoService {

    @Autowired
    private TipoDocumentoRepository tipoDocumentoRepository;

    /**
     * 
     * @return ResponseEntity con la lista de tipos de documento
     */
    @Override
    public ResponseEntity ListAll() {
        

        ArrayList<TipoDocumento> documentos = (ArrayList<TipoDocumento>) tipoDocumentoRepository.findAll();
        ArrayList<TipoDocumentoDTO> retorno = new ArrayList<TipoDocumentoDTO>();
        ListaJson<TipoDocumentoDTO> lista = new ListaJson<TipoDocumentoDTO>();

        for (TipoDocumento p : documentos) {
            TipoDocumentoDTO pdto = new TipoDocumentoDTO();
            pdto.setId("" + p.getId());
            pdto.setNombre(p.getNombre());
            retorno.add(pdto);
        }
        lista.setLista(retorno);
        return ResponseEntity.ok(lista);
    }

}
