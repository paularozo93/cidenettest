/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.empleados.service;

import com.cidenet.empleados.dto.EmpleadoDTO;
import com.cidenet.empleados.dto.json.FindJson;
import org.springframework.http.ResponseEntity;

/**
 *
 * @author jerson
 */
public interface EmpleadoService {
    /**
     * 
     * @param e EmpleadoDTO a añadir
     * @return ResponseEntity con mensaje de confirmacion
     */
    public ResponseEntity add(EmpleadoDTO e);
    
    /**
     * 
     * @return ResponseEntity con los empleados encontrados
     */
    public ResponseEntity list();
    
    /**
     * 
     * @param e empleado a modificar
     * @return ResponseEntity confimacion 
     */
    public ResponseEntity edit(EmpleadoDTO e);
    
    /**
     * 
     * @param f campo de busqueda con el empleado a buscar
     * @return ResponseEntity con confirmacion
     */
    public ResponseEntity delete(FindJson f);
    
    /**
     * 
     * @param f campo de busqueda con el id del empleado a buscar
     * @return ResponseEntity con el empleado
     */
    public ResponseEntity find(FindJson f);
}
