/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.empleados.service;

import org.springframework.http.ResponseEntity;

/**
 *
 * @author jerson
 */
public interface TipoDocumentoService {

    /**
     * 
     * @return ResponseEntity con la lista de Tipos de documento
     */
    public ResponseEntity ListAll();
}
