/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.empleados.service.impl;

import com.cidenet.empleados.dto.EmpleadoDTO;
import com.cidenet.empleados.dto.json.FindJson;
import com.cidenet.empleados.dto.json.ListaJson;
import com.cidenet.empleados.dto.json.MessageJson;
import com.cidenet.empleados.entity.Empleado;
import com.cidenet.empleados.entity.Pais;
import com.cidenet.empleados.repository.AreaRepository;
import com.cidenet.empleados.repository.EmpleadoRepository;
import com.cidenet.empleados.repository.PaisRepository;
import com.cidenet.empleados.repository.TipoDocumentoRepository;
import com.cidenet.empleados.service.EmpleadoService;
import com.cidenet.empleados.util.Util;
import java.util.ArrayList;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/**
 *
 * @author jerson
 */
@Service
public class EmpleadoServiceImpl implements EmpleadoService {

    @Autowired
    private AreaRepository areaRepository;
    @Autowired
    private PaisRepository paisRepository;
    @Autowired
    private TipoDocumentoRepository tipDocumentoRepository;
    @Autowired
    private EmpleadoRepository empleadoRepository;

    /**
     * @param f json con id a buscar
     * @return ResponseEntity con el usuario a buscar
     * 
     */
    @Override
    public ResponseEntity find(FindJson f) {

        String campo = f.getValue1();

        int id = Integer.parseInt(campo);
        Empleado e = empleadoRepository.findById(id);
        
        return ResponseEntity.ok(Util.EmpleadoEntityToDTO(e));
    }

    /**
     * @param f value 1 id a borrar , value2 sin usar
     * @return responseEntity con confirmacion
     */
    @Override
    public ResponseEntity delete(FindJson f) {

        Empleado e = new Empleado();
        e.setId(Integer.parseInt(f.getValue1()));
        empleadoRepository.delete(e);
        MessageJson msg = new MessageJson();
        msg.setMensaje("ok");
        return ResponseEntity.ok(msg);

    }

    /**
     * retorna una de lista los empleados existentes
     *
     * @return ResponseEntity con arraylist<EmpleadoDTO> con la lista de empleados
     */
    @Override
    public ResponseEntity list() {
       

        ArrayList<Empleado> empleados = (ArrayList<Empleado>) empleadoRepository.findAll();

        ArrayList<EmpleadoDTO> empls = Util.agrupar(empleados);
        ListaJson<EmpleadoDTO> lista = new ListaJson<EmpleadoDTO>();
        lista.setLista(empls);
        return ResponseEntity.ok(lista);
    }

    /**
     * registra el nuevo empleado
     *
     * @param empleado
     * @return ResponseEntity
     */
    @Override
    public ResponseEntity add(EmpleadoDTO empleado) {

        /*
        crea un entity con base a empleado dto con los campos que pueden ser 
        asignados inmediatamente
         */
        Empleado emp = Util.EmpleadoDTOToEntity(empleado);

        //consulta el pais del empleado  y le asigna la id
        Pais p = paisRepository.findById(Integer.parseInt(empleado.getPais()));
        emp.setIdPais(p);

        //genera un correo con los datos del empleado y el dominio del pais
        String email = generateMail(empleado, p);
        System.out.println(email);
        emp.setCorreo(email);

        empleadoRepository.save(emp);

        MessageJson msg = new MessageJson();
        msg.setMensaje("ok");
        return ResponseEntity.ok(msg);

    }

    /**
     * actualiza un empleado , genera un nuevo correo si es necesario
     *
     * @param empleado , datos del empleado a actualizar
     * @return ResponseEntity una respuesta que contiene los datos del empleado
     * actualizados
     */
    @Override
    public ResponseEntity edit(EmpleadoDTO empleado) {
        //convierte empleado deto a entity
        Empleado emp = Util.EmpleadoDTOToEntity(empleado);

        //obtiene empleado original para comparar
        Empleado original = empleadoRepository.findById(Integer.parseInt(empleado.getId()));

        //si el empleado a editar no existe retorna null
        if (original == null) {
            return null;
        }

        //asigna pais al empleado
        emp.setId(original.getId());
        emp.setIdPais(original.getIdPais());

        if (empleado.getEstado().equals("false")) {
            emp.setEstado(false);
        }
        emp.setCorreo(original.getCorreo());

        //asigna fecha de actualizacion al empleado
        emp.setFechaHoraActualizacion(new Date());

        //genera un nuevo correo si hubo cambios en el nombre del usuario 
        if (!emp.getPrimerNombre().equals(original.getPrimerNombre())
                || !emp.getPrimerApellido().equals(original.getPrimerApellido())) {
            String correo = generateMail(empleado, original.getIdPais());
            emp.setCorreo(correo);
            System.out.print(correo);
        }

        //guarda los cambios
        empleadoRepository.save(emp);
        MessageJson msg = new MessageJson();
        msg.setMensaje("ok");
        return ResponseEntity.ok(msg);
    }

    /**
     * genera un correo que cumpla con los requisitos de:
     * primer_nombre.primer_apellido@dominio si ya existe un email igual se
     * genera de la siguente forma primer_nombre.primer_apellido.numero@dominio
     * siendo numero la cantidad de emails iguales
     *
     * @param empleado
     * @param p , Pais , recibe datos del usuario y el pais
     * @return cadena de texto con el email generado
     */
    private String generateMail(EmpleadoDTO empleado, Pais p) {

        String firstnom = empleado.getPrimerNombre();
        String firsapel = empleado.getPrimerApellido();

        int count = 0;

        long cantidad = empleadoRepository.countByPrimerNombreAndPrimerApellido(firstnom, firsapel);
        System.out.println("cantidad:" + cantidad);

        String email = Util.buildEmail(firstnom, firsapel, p.getDominio(), cantidad);

        return email;

    }

}
