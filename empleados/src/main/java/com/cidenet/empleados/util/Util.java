/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.empleados.util;

import com.cidenet.empleados.dto.EmpleadoDTO;
import com.cidenet.empleados.entity.Area;
import com.cidenet.empleados.entity.Empleado;
import com.cidenet.empleados.entity.TipoDocumento;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author jerson
 */
public class Util {

    /**
     * convierte entity empleado a empleadoDTO
     *
     * @param emp , entity a convertir a dto
     * @return EmpleadoDTO , dto generado
     */
    public static EmpleadoDTO EmpleadoEntityToDTO(Empleado emp) {
        EmpleadoDTO e = new EmpleadoDTO();
        e.setId("" + emp.getId());
        e.setPrimerNombre(emp.getPrimerNombre());
        e.setOtrosNombres(emp.getOtrosNombres());
        e.setPrimerApellido(emp.getPrimerApellido());
        e.setSegundoApellido(emp.getSegundoApellido());
        e.setCorreo(emp.getCorreo());
        e.setEstado("" + emp.getEstado());
        e.setNumeroIdentificacion(emp.getNumeroIdentificacion());
        e.setTipoDocumento(emp.getIdDocumento().getNombre());
        e.setArea(emp.getIdArea().getNombre());
        e.setPais(emp.getIdPais().getNombre());
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        e.setFechaIngreso(format.format(emp.getFechaIngreso()));
        e.setFechaHoraRegistro(emp.getFechaHoraRegistro());
        e.setFechaHoraActualizacion(emp.getFechaHoraActualizacion());

        return e;

    }

    /**
     * convierte un empleadoDTO a Empleado
     *
     * @param empleado empleadoDTO a convertir
     * @return Empleado , entity creadda con base al dto
     */
    public static Empleado EmpleadoDTOToEntity(EmpleadoDTO empleado) {

        Empleado empl = new Empleado();

        empl.setPrimerNombre(empleado.getPrimerNombre());
        empl.setOtrosNombres(empleado.getOtrosNombres());
        empl.setPrimerApellido(empleado.getPrimerApellido());
        empl.setSegundoApellido(empleado.getSegundoApellido());
        empl.setNumeroIdentificacion(empleado.getNumeroIdentificacion());
        empl.setEstado(true);

        Area empl_area = new Area();
        empl_area.setId(Integer.parseInt(empleado.getArea()));

        TipoDocumento empl_tipo = new TipoDocumento();
        empl_tipo.setId(Integer.parseInt(empleado.getTipoDocumento()));

        empl.setIdArea(empl_area);
        empl.setIdDocumento(empl_tipo);

        empl.setFechaIngreso(Util.textToDate(empleado.getFechaIngreso()));

        empl.setFechaHoraRegistro(new Date());

        System.out.println("current: " + empl.getFechaIngreso());
        System.out.println(empl.getFechaHoraRegistro());

        return empl;

    }

    /**
     * genera email con base al nombre , apellido , dominio y un indice
     *
     * @param nombre , cadena apellido , cadena dominio , numero indice
     * @param apellido
     * @param dominio
     * @param indice
     * @return Stringcadena con el email
     */
    public static String buildEmail(String nombre, String apellido, String dominio, long indice) {
        String firs = Util.removeSpaces(nombre) + "." + Util.removeSpaces(apellido);
        if (indice > 0) {
            firs = firs + "." + indice;
        }
        return firs + "@" + dominio;
    }

    /**
     * convierte texto a date
     *
     * @param date texto con formato "dd-MM-yyyy"
     * @return Date fecha en un Date
     */
    public static Date textToDate(String date) {
        Date fecha = null;
        try {
            fecha = new SimpleDateFormat("yyyy-mm-dd").parse(date);
        } catch (ParseException ex) {
            LocalDate fech = LocalDate.parse(date);
            fecha = Date.from(fech.atStartOfDay(ZoneId.systemDefault()).toInstant());
        }
        return fecha;
    }

    /**
     * remueve los espacios de una cadena
     *
     * @param val cadena a remover espacios
     * @return cadena sin espacios
     */
    public static String removeSpaces(String val) {
        return val.replaceAll("\\s+", "");
    }

    /**
     * @param empleados lista de entitys de empleado
     * @return ArrayList<EmpleadoDTO> lista de empleadoDTO
     */
    public static ArrayList<EmpleadoDTO> agrupar(ArrayList<Empleado> empleados) {
        ArrayList<EmpleadoDTO> empls = new ArrayList<EmpleadoDTO>();

        for (Empleado D : empleados) {
            EmpleadoDTO e = Util.EmpleadoEntityToDTO(D);
            empls.add(e);
        }
        return empls;
    }
}
