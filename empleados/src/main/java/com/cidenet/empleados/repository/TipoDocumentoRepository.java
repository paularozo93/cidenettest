package com.cidenet.empleados.repository;

import org.springframework.data.repository.CrudRepository;
import com.cidenet.empleados.entity.TipoDocumento;

public interface TipoDocumentoRepository extends CrudRepository<TipoDocumento, Long> {

    /**
     * 
     * @param id del tipo de documento a buscar
     * @return TipoDocumento a buscar
     */
    TipoDocumento findById(Integer id);
}
