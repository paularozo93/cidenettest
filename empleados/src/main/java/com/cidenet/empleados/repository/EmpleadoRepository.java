package com.cidenet.empleados.repository;

import org.springframework.data.repository.CrudRepository;
import com.cidenet.empleados.entity.Empleado;

import java.util.List;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface EmpleadoRepository extends CrudRepository<Empleado, Long> {

    /**
     * 
     * @param id del empleado a buscar
     * @return Empleado encontrado
     */
    Empleado findById(Integer id);

    
 
/**
 * 
 * @param correo correo del usuario a buscar
 * @return cantidad de correos iguales
 */
    long countByCorreo(String correo);

    /**
     * 
     * @param primernombre primer nombre del usuario a buscar
     * @param primerapellido primer apellido del usuario a buscar
     * @return cantidad de empleados que tienen primer nombre y primer apellido correspondientes a la busqueda
     */
    long countByPrimerNombreAndPrimerApellido(String primernombre, String primerapellido);

}
