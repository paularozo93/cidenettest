package com.cidenet.empleados.repository;

import org.springframework.data.repository.CrudRepository;
import com.cidenet.empleados.entity.Area;

public interface AreaRepository extends CrudRepository<Area, Long> {

    /**
     * 
     * @param id id a buscar
     * @return Ara encontrada
     */
    Area findById(Integer id);

    /**
     * 
     * @param nombre a buscar
     * @return Area encontrada
     */
    Area findByNombre(String nombre);

}
