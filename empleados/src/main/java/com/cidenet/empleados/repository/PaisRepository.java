package com.cidenet.empleados.repository;

import org.springframework.data.repository.CrudRepository;
import com.cidenet.empleados.entity.Pais;

public interface PaisRepository extends CrudRepository<Pais, Long> {

    /**
     * 
     * @param id del pais a buscar
     * @return pais encontrado
     */
    Pais findById(Integer id);
}
