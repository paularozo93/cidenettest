/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.empleados.dto.json;

import com.sun.istack.NotNull;
import javax.validation.constraints.NotBlank;
import lombok.Data;

/**
 *
 * @author jerson usado para operaciones de busqueda value1 suele ser el valor
 * con base al que se hara la busqueda value2 sera el atributo con el que se
 * realiza la busqueda
 */
@Data
public class FindJson {

    @NotNull
    @NotBlank(message = "el campo nombre es obligatorio")
    private String value1;
    private String value2;

}
