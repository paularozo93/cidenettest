package com.cidenet.empleados.dto;

import lombok.Data;

@Data
public class AreaDTO {

    private String id;
    private String nombre;

}
