/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.empleados.dto;

import lombok.Data;

/**
 *
 * @author jerson
 */
@Data
public class TipoDocumentoDTO {

    private String id;
    private String nombre;

}
