package com.cidenet.empleados.dto;

import lombok.Data;

@Data
public class PaisDTO {

    private String id;
    private String nombre;

}
