package com.cidenet.empleados.dto.json;

import java.util.ArrayList;
import lombok.Data;

@Data
public class ListaJson<T> {

    ArrayList<T> lista;

}
