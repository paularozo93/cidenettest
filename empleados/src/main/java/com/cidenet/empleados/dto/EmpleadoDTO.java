package com.cidenet.empleados.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.sun.istack.NotNull;
import java.util.Date;
import lombok.Data;

@Data
public class EmpleadoDTO {

    private String id;

    @NotNull
    @NotBlank(message = "el campo nombre es obligatorio")
    @Size(max = 20, message = "el nombre no puede superar los 20 caracteres")
    @Pattern(regexp = "^[A-Z]+$", message = "el campo nombre solo puede contener texto con letras mayusculas y sin acentos ni letra ñ")
    private String primerNombre;

    @Size(max = 20, message = "otros nombres no puede superar los 20 caracteres")
    @Pattern(regexp = "^$|^[A-Z\\s]+$", message = "otros nombres solo pueden contener texto con letras mayusculas y sin acentos ni letra ñ")
    private String otrosNombres;

    @NotNull
    @NotBlank(message = "el campo primer apellido es obligatorio")
    @Size(max = 20, message = "el primer apellido no puede superar los 20 caracteres")
    @Pattern(regexp = "^[A-Z]+$", message = "el primer apellido solo puede contener texto con letras mayusculas y sin acentos ni letra ñ")
    private String primerApellido;

    @NotNull
    @NotBlank(message = "el campo segundo apellido es obligatorio")
    @Size(max = 20, message = "el segundo apellido no puede superar los 20 caracteres")
    @Pattern(regexp = "^[A-Z]+$", message = "el segundo apellido solo pueden contener texto con letras mayusculas y sin acentos ni letra ñ")
    private String segundoApellido;

    @NotNull
    @NotBlank(message = "el campo numero de identificacion es obligatorio")
    @Size(max = 20, message = "no puede superar los 20 caracteres")
    @Pattern(regexp = "^[A-Za-z0-9-]+$", message = "solo pueden contener caracteres alphanumericos")
    private String numeroIdentificacion;

    @NotNull
    @NotBlank(message = "el campo pais es obligatorio")
    private String pais;

    private String correo;

    @NotNull
    @NotBlank(message = "el campo tipo de documento es obligatorio")
    private String tipoDocumento;

    @NotNull
    @NotBlank(message = "el campo estado es obligatorio")
    private String estado;

    @NotNull
    @NotBlank(message = "el campo fecha es obligatorio")
    private String fechaIngreso;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date fechaHoraRegistro;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date fechaHoraActualizacion;

    @NotNull
    @NotBlank(message = "el campo area es obligatorio")
    private String area;

 
   
    
}
