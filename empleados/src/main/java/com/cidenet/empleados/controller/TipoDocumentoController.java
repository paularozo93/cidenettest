/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.empleados.controller;

import com.cidenet.empleados.service.TipoDocumentoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jerson
 */
@RestController
@RequestMapping("/documento")
@CrossOrigin
public class TipoDocumentoController {

    @Autowired
    private TipoDocumentoService tipoDocumentoService;

    /**
     * 
     * @return ResponseEntity con la lista de tipos de documento
     */
    @PostMapping("/listar")
    public ResponseEntity listar() {
        return tipoDocumentoService.ListAll();

    }
}
