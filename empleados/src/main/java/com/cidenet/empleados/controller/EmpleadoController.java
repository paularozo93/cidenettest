package com.cidenet.empleados.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cidenet.empleados.dto.EmpleadoDTO;
import com.cidenet.empleados.dto.json.FindJson;
import com.cidenet.empleados.service.EmpleadoService;

@RestController
@RequestMapping("/empleado")
@CrossOrigin
public class EmpleadoController {

    @Autowired
    private EmpleadoService empleadoService;

    /**
     * 
     * @param empleado a registrar
     * @return ResponseEntity con mensaje de confirmacion
     */
    @PostMapping("/new")
    public ResponseEntity registry(@Valid @RequestBody EmpleadoDTO empleado) {
        return empleadoService.add(empleado);

    }
/**
 * 
 * @return ResponseEntity con lista de empleados
 */
    @PostMapping("/list")
    public ResponseEntity list() {
        return empleadoService.list();
    }

    /**
     * 
     * @param empleado a  modificar
     * @return ResponseEntity con confirmacion
     */
    @PostMapping("/edit")
    public ResponseEntity edit(@Valid @RequestBody EmpleadoDTO empleado) {
        return empleadoService.edit(empleado);
    }

    /**
     * 
     * @param find campo de busqueda con la id del epleado a eliminar
     * @return  mensaje de confirmacion
     */
    @PostMapping("/delete")
    public ResponseEntity delete(@RequestBody FindJson find) {
        return empleadoService.delete(find);
    }

    /**
     * 
     * @param find campo con la id del empleado a buscar
     * @return ResponseEntity con el empleado encontrado
     */
    @PostMapping("/find")
    public ResponseEntity find(@RequestBody FindJson find) {
        return empleadoService.find(find);
    }

}
