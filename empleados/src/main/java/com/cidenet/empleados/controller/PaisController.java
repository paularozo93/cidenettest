package com.cidenet.empleados.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cidenet.empleados.service.PaisService;

@RestController
@RequestMapping("/pais")
@CrossOrigin
public class PaisController {

    @Autowired
    private PaisService paisService;

    /**
     * 
     * @return ResponseEntity con la lista de paises
     */
    @PostMapping("/listar")
    public ResponseEntity listar() {
        return paisService.ListAll();

    }

}
