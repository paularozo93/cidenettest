package com.cidenet.empleados.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cidenet.empleados.service.AreaService;

@RestController
@RequestMapping("/area")
@CrossOrigin
public class AreaController {

    @Autowired
    private AreaService areaService;
    
    /**
     * 
     * @return ResponseEntity con la lista de areas
     */
    @PostMapping("/listar")
    public ResponseEntity listar() {

        return areaService.ListAll();
    }

}
